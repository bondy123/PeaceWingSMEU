下一个重大版本，和平之翼Java代码生成器SMEU 3.0版，研发代号(乌篷船 Black Awning Boat)正在Beta测试中，在附件中提供war包下载。此版本支持使用Excel模板的代码生成，支持初始数据导入。欢迎测试，欢迎试用。

乌篷船支持华丽的Excel模板代码生成和初始数据导入，软件新增Excel生成界面和示例Excel模板。界面和Excel模板如下图。

Excel模板：
![输入图片说明](https://gitee.com/uploads/images/2017/1205/140634_26b1560b_1203742.png "exceltemplate_1.png")

![输入图片说明](https://gitee.com/uploads/images/2017/1205/140649_e5780e3d_1203742.png "exceltemplate_2.png")

Excel生成界面：
![输入图片说明](https://gitee.com/uploads/images/2017/1205/140731_0433c855_1203742.png "Bab_2.png")

传统的SGS（标准生成器脚本）生成界面：
![输入图片说明](https://gitee.com/uploads/images/2017/1205/140804_dab88aa6_1203742.png "Bab_1.png")

最新稳定版本和平之翼代码生成器SMEU 2.0 正式版。
2.0版 Code Name： Chinese Sailboat 红头船 致敬伟大的中国航海传统。
和平之翼代码生成器SMEU 2.0正式版，一键支持下拉列表和多对多，已支持Oracle数据库。
本版是和平之翼代码生成器SMEU 2.0正式版（红头船）。
SMEU技术栈支持JQuery Easy UI,Spring MVC4,spring4, MyBatis 3。
本版支持下拉列表，使用者只需要在域对象相应的外键字段设定dropdown:DomainName fieldName;
即下拉列表：外键域名 字段名，即可一键支持下拉列表（外键）。
本版支持多对多关系，只要在多对多关系的主域对象中定义了
manytomanyslave:slaveDomainName即可在生成的功能和数据库定义中支持了两者的多对多
关系。
和平之翼代码生成器是动词算子式Java通用代码生成器，是无垠式代码生成器的第二代。
修正了一些以前的Bug，并作了功能增强和文档更新。希望您喜欢。
2.0支持Oracle数据库，您只需要定义dbtype:oracle即可支持Oracle数据库，详细情况请看相关示例。
