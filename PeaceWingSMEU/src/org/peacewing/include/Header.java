package org.peacewing.include;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.domain.DragonHideStatement;
import org.peacewing.domain.Include;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.utils.WriteableUtil;

public class Header extends Include{
	protected String title = "PeaceWing Generate Results";
	protected String year = "2016";
	
	public Header(){
		super();
		this.fileName = "header.jsp";
		this.packageToken = "";
	}
	
	public Header(String title, String year){
		super();
		this.fileName = "dbconfig.xml";
		this.packageToken = "";
		this.title = title;
		this.year = year;
	}

	@Override
	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		StatementList list = new StatementList();
		sb.append("<div id=\"header_wide\">\n");
		sb.append("<div id=\"logo\">\n");
		sb.append("\t<h1><a href=\"../pages/index.html\">"+this.title+"</a></h1>\n");
		sb.append("\t<h2><a href=\"../pages/index.html\">Serving Guests Since "+this.year+"</a></h2>\n");
		sb.append("</div>\n");
		sb.append("<!-- end div#logo -->\n");
		sb.append("<!-- end div#menu -->\n");
		sb.append("</div>\n");
		return sb.toString();
	}
	
	@Override
	public StatementList getStatementList(long serial, int indent) {
		return getStatementList(serial,indent,false);
	}	
	
	public StatementList getStatementList(long serial, int indent,boolean isNotParent) {
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,indent,"<div id=\"header_wide\">"));
		list.add(new Statement(2000L,indent+1,"<div id=\"logo\">"));
		list.add(new DragonHideStatement(3000L,indent+2,"<h1><a href=\"pages/index.html\">"+this.title+"</a></h1>",!isNotParent));
		list.add(new DragonHideStatement(3000L,indent+2,"<h1><a href=\"../pages/index.html\">"+this.title+"</a></h1>",isNotParent));
		list.add(new DragonHideStatement(4000L,indent+2,"<h2><a href=\"pages/index.html\">Serving Guests Since "+this.year+"</a></h2>",!isNotParent));
		list.add(new DragonHideStatement(4000L,indent+2,"<h2><a href=\"../pages/index.html\">Serving Guests Since "+this.year+"</a></h2>",isNotParent));
		list.add(new Statement(5000L,indent+1,"</div>"));
		list.add(new Statement(6000L,indent,"<!-- end div#logo -->"));
		list.add(new Statement(7000L,indent,"<!-- end div#menu -->"));
		list.add(new Statement(8000L,indent,"</div>"));
		StatementList myList = WriteableUtil.merge(list);
		myList.setSerial(serial);
		return myList;
	}
}