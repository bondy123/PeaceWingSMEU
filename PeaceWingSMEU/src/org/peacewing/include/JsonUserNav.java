package org.peacewing.include;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Include;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;
import org.peacewing.verb.ListAllByPage;

public class JsonUserNav extends Include{
	public JsonUserNav(List<Domain> domains){
		super();
		this.fileName = "jsonusernav.jsp";
		this.packageToken = "";
		this.allDomainList.addAll(domains);
	}

	@Override
	public String generateIncludeString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<!-- Common Navigation Panel for our site -->\n");
		sb.append("<li id=\"submenu\">\n");
		sb.append("\t<h2><a href=\"../index.html\">Homepage</a></h2><br/>\n");
		sb.append("\t<h2><a>Select an option</a></h2>\n");
		sb.append("\t<ul>\n");
		for (Domain d: this.allDomainList){
			sb.append("\t\t<li><a href=\""+"../pages/"+d.getPlural().toLowerCase()+".html\">"+d.getCapFirstDomainName()+"</a></li>\n");   
		}
		sb.append("\t</ul>\n");
		sb.append("</li>\n");
		return sb.toString();
	}

	@Override
	public StatementList getStatementList(long serial, int indent) {
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,indent,"<!-- Common Navigation Panel for our site -->\n"));
		list.add(new Statement(2000L,indent,"<li id=\"submenu\">"));
		list.add(new Statement(3000L,indent+1,"<h2><a href=\"../index.html\">Homepage</a></h2><br/>"));
		list.add(new Statement(4000L,indent+1,"<h2><a>Select an option</a></h2>"));
		list.add(new Statement(6000L,indent+1,"<ul>"));
		long myserial = 7000L;
		for (Domain d: this.allDomainList){
			list.add(new Statement(myserial,indent+2,"<li><a href=\""+"../pages/"+d.getPlural().toLowerCase()+".html\">"+d.getCapFirstDomainName()+"</a></li>"));
			myserial+=1000L;
		}
		list.add(new Statement(myserial,indent+1,"</ul>\n"));
		list.add(new Statement(myserial+1000L,indent,"</li>\n"));
		StatementList myList = WriteableUtil.merge(list);
		myList.setSerial(serial);
		return myList;
	}
}
