package org.peacewing.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;

public class Noun extends Domain implements Serializable{
	private static final long serialVersionUID = -4771766410591672757L;
	protected String nounName;
	public boolean supports(Noun noun){
		List<Field> fixedFields = noun.getFixedFields();
		List<Field> myfixedFields = getFixedFields();
		
OUTER: for (Field f:fixedFields){
			for (Field mf:myfixedFields){
				if(mf.getFieldName().equals(f.getFieldName())&& mf.getFieldType().equals(f.getFieldType())) continue OUTER;
			}
			return false;
		}
		return true;
	}
	
	public Field findFixedFieldByFieldName(String fieldName){
		for (Field f :getFields()){
			if (f.isFixed() == true && f.getFieldName().equals(fieldName)){
				return f;
			}
		}
		return null;
	}
	
	public Noun(String nounName){
		super();
		this.nounName = nounName;
	}
	
	public void setFixedFeilds(List<Field> fixedFields){
		for (Field f:fixedFields){
			f.setFixed(true);
			this.addField(f);
		}
	}
	
	public List<Field> getFixedFields(){
		Set<Field> all = this.getFields();
		List<Field> retList = new ArrayList<Field>();
		for (Field f:all){
			if (f.isFixed()) retList.add(f);
		}
		return retList;
	}
}
