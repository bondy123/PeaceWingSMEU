package org.peacewing.core;

public interface ICloneable extends Cloneable{
	public Object clone();
}
