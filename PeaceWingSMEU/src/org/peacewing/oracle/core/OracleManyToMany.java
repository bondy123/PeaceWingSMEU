package org.peacewing.oracle.core;

import org.peacewing.domain.Domain;
import org.peacewing.domain.ManyToMany;
import org.peacewing.easyui.EasyUIManyToManyTemplate;
import org.peacewing.generator.MysqlTwoDomainsDBDefinitionGenerator;
import org.peacewing.oracle.complexverb.Assign;
import org.peacewing.oracle.complexverb.ListMyActive;
import org.peacewing.oracle.complexverb.ListMyAvailableActive;
import org.peacewing.oracle.complexverb.Revoke;
import org.peacewing.utils.StringUtil;

public class OracleManyToMany extends ManyToMany{
	protected Assign assign;
	protected Revoke revoke;
	protected ListMyActive listMyActive;
	protected ListMyAvailableActive listMyAvailableActive;
	
	public OracleManyToMany(){
		super();
	}
	
	public OracleManyToMany(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		this.assign = new Assign(master,slave);
		this.revoke = new Revoke(master,slave);
		this.listMyActive = new ListMyActive(master,slave);
		this.listMyAvailableActive = new ListMyAvailableActive(master,slave);
		this.euTemplate = new EasyUIManyToManyTemplate(master,slave);
	}
	public void setMaster(Domain master) {
		this.master = master;
	}
	public Domain getSlave() {
		return slave;
	}
	public void setSlave(Domain slave) {
		this.slave = slave;
	}

	public void setListMyAvailableActive(ListMyAvailableActive listMyAvailableActive) {
		this.listMyAvailableActive = listMyAvailableActive;
	}
	public EasyUIManyToManyTemplate getEuTemplate() {
		return euTemplate;
	}
	public void setEuTemplate(EasyUIManyToManyTemplate euTemplate) {
		this.euTemplate = euTemplate;
	}
	public Domain getMaster() {
		return master;
	}
	public String getStandardName(){
		return "Link"+this.master.getStandardName()+this.slave.getStandardName();
	}
	@Override
	public int compareTo(ManyToMany o) {
		return this.getStandardName().compareTo(o.getStandardName());
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getText(){
		if (StringUtil.isBlank(this.master.getLabel())){
			return "Link "+this.master.getStandardName()+" "+this.slave.getStandardName();
		}
		else return "链接"+this.master.getText()+this.slave.getText();
	}
	
	public MysqlTwoDomainsDBDefinitionGenerator toTwoDBGenerator(){
		MysqlTwoDomainsDBDefinitionGenerator mtg = new MysqlTwoDomainsDBDefinitionGenerator(this.master,this.slave);
		return mtg;
	}

	public Assign getOracleAssign() {
		return assign;
	}

	public void setOracleAssign(Assign assign) {
		this.assign = assign;
	}

	public Revoke getOracleRevoke() {
		return revoke;
	}

	public void setOracleRevoke(Revoke revoke) {
		this.revoke = revoke;
	}

	public ListMyActive getOracleListMyActive() {
		return listMyActive;
	}

	public void setOracleListMyActive(ListMyActive listMyActive) {
		this.listMyActive = listMyActive;
	}

	public ListMyAvailableActive getOracleListMyAvailableActive() {
		return listMyAvailableActive;
	}
}
