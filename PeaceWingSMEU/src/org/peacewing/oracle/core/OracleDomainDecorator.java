package org.peacewing.oracle.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;


public class OracleDomainDecorator {
	public static Domain decorateOracleDomain(Domain d){
		if (d!=null){
			Domain d2 = (Domain) d.deepClone();
			for (Field f: d2.getFields()){
				if ("Long".equalsIgnoreCase(f.getClassType().getTypeName())) f.setFieldType("String");
				else if ("Boolean".equalsIgnoreCase(f.getClassType().getTypeName())) f.setFieldType("Integer");
			}
			return d2;
		} else {
			return null;
		}
	}
	
	public static List<Domain> decorateOracleDomainList(List<Domain> domainList){
		List<Domain> result = new ArrayList<Domain>();
		for (Domain d:domainList){
			Domain d2 = decorateOracleDomain(d);
			result.add(d2);
		}
		return result;
	}
	
	public static Set<Domain> decorateOracleDomainSet(Set<Domain> domainSet){
		Set<Domain> result = new TreeSet<Domain>();
		for (Domain d:domainSet){
			Domain d2 = decorateOracleDomain(d);
			result.add(d2);
		}
		return result;
	}
}
