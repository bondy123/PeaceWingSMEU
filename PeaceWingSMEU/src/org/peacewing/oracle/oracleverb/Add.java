package org.peacewing.oracle.oracleverb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Dropdown;
import org.peacewing.domain.Field;
import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.StatementList;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.generator.NamedStatementListGenerator;
import org.peacewing.oracle.generator.MybatisOracleSqlReflector;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class Add extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));		
		method.setNoContainer(true);
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,1,"<insert id=\""+StringUtil.lowerFirst(this.getVerbName())+"\" parameterType=\""+this.domain.getFullName()+"\">"));
		list.add(new Statement(200L,2, MybatisOracleSqlReflector.generateInsertSql(domain)));
		list.add(new Statement(300L,1,"</insert>"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}
	
	

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("void"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		return method;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return generateDaoImplMethod().generateMethodString();
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception  {
		Method method = new Method();
		method.setStandardName("add"+StringUtil.capFirst(this.domain.getStandardName()));
		method.setReturnType(new Type("Boolean"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("org.springframework.transaction.annotation.Transactional");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addMetaData("Override");		
		
		method.addSignature(new Signature(1, StringUtil.lowerFirst(this.domain.getStandardName()), this.domain.getType()));
				
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(NamedStatementGenerator.generateAddServiceImpl(1000L, 2, this.domain, InterVarUtil.DB.dao));
		list.add(new Statement(2000L,2,"return true;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception  {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public Add(){
		super();
		this.setLabel("新增");
	}
	
	public Add(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("Add"+StringUtil.capFirst(this.domain.getStandardName()));
		this.setLabel("新增");
	}



	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		return null;
	}



	@Override
	public Method generateFacadeMethod() throws Exception {
		Method method = new Method();
		method.setStandardName("add"+this.domain.getCapFirstDomainName());
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getLowerFirstDomainName(),this.domain.getType(),this.domain.getPackageToken(),"RequestBody"));
		method.addMetaData("RequestMapping(value = \"/add"+this.domain.getCapFirstDomainName()+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
		wlist.add(NamedStatementGenerator.getCallServiceMethod(2000L, 2, service, generateServiceMethodDefinition()));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndNull(3000L, 2, resultMap));
		wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName()+";"));			
		method.setMethodStatementList(WriteableUtil.merge(wlist));		
		return method;
	}
	

	@Override
	public String generateFacadeMethodString()throws Exception {
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}



	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception {
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}



	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		JavascriptBlock block = new JavascriptBlock();
		block.setSerial(100);
		block.setStandardName("delete"+domain.getCapFirstDomainName());
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,0, "{"));
		sl.add(new Statement(2000,1, "text:'新增',"));
		sl.add(new Statement(3000,1, "iconCls:'icon-add',"));
		sl.add(new Statement(4000,1, "handler:function(){"));
		sl.add(new Statement(5000,2, "$('#wadd"+this.domain.getCapFirstDomainName()+"').window('open');"));
		sl.add(new Statement(6000,1, "}"));
		sl.add(new Statement(7000,0, "}"));
		block.setMethodStatementList(sl);
		return block;			
	}



	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}



	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}
	
	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		Domain domain = this.domain;
		JavascriptMethod method = new JavascriptMethod();
		method.setSerial(200);
		method.setStandardName("add"+domain.getCapFirstDomainName());
		
		StatementList sl = new StatementList();
		sl.add(new Statement(1000,1, "if ($(\"#ff\").form(\"validate\")) {"));
		sl.add(new Statement(2000,2, "$.ajax({"));
		sl.add(new Statement(3000,3, "type: \"post\","));
		sl.add(new Statement(4000,3, "url: \"../facade/"+domain.getLowerFirstDomainName()+"Facade/add"+domain.getCapFirstDomainName()+"\","));
		sl.add(new Statement(5000,3, "data: JSON.stringify({"));
		long serial = 6000;
		for (Field f: domain.getFieldsWithoutId()){
			if (f instanceof Dropdown){
				sl.add(new Statement(serial,4, f.getLowerFirstFieldName()+":$(\"#ff\").find(\"#"+f.getLowerFirstFieldName()+"\").combobox(\"getValue\"),"));
			} else if (f.getFieldType().equalsIgnoreCase("boolean")||f.getFieldName().equals(domain.getActive().getFieldName())){
				sl.add(new Statement(serial,4, f.getLowerFirstFieldName()+":$(\"#ff\").find(\"input[name='"+f.getLowerFirstFieldName()+"']:checked\").val(),"));								
			} else {
				sl.add(new Statement(serial,4, f.getLowerFirstFieldName()+":$(\"#ff\").find(\"#"+f.getLowerFirstFieldName()+"\").val(),"));
			}
			serial+=1000;
		}				
		sl.add(new Statement(serial,3, "}),"));
		sl.add(new Statement(serial+1000,3, "dataType: 'json',"));
		sl.add(new Statement(serial+2000,3, "contentType:\"application/json;charset=UTF-8\","));
		sl.add(new Statement(serial+3000,3, "success: function(data, textStatus) {"));
		sl.add(new Statement(serial+4000,4, "if (data.success) {"));
		sl.add(new Statement(serial+5000,5, "$('#ff').form('clear');"));
		sl.add(new Statement(serial+6000,5, "$(\"#ff\").find(\"input[name='"+domain.getActive().getLowerFirstFieldName()+"']\").get(0).checked = true;"));
		sl.add(new Statement(serial+7000,5, "$(\"#wadd"+domain.getCapFirstDomainName()+"\").window('close');"));
		sl.add(new Statement(serial+8000,5, "$(\"#dg\").datagrid(\"load\");"));
		sl.add(new Statement(serial+9000,4, "}"));
		sl.add(new Statement(serial+10000,4, "},"));
		sl.add(new Statement(serial+11000,3, "complete : function(XMLHttpRequest, textStatus) {"));
		sl.add(new Statement(serial+12000,3, "},"));
		sl.add(new Statement(serial+13000,3, "error : function(XMLHttpRequest,textStatus,errorThrown) {"));
		sl.add(new Statement(serial+14000,4, "alert(\"Error:\"+textStatus);"));
		sl.add(new Statement(serial+15000,4, "alert(errorThrown.toString());"));
		sl.add(new Statement(serial+16000,3, "}"));
		sl.add(new Statement(serial+17000,2, "});"));
		sl.add(new Statement(serial+18000,1, "}"));
		
		method.setMethodStatementList(sl);
		return method;	
	}



	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodString();
	}



	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodStringWithSerial();
	}

}
