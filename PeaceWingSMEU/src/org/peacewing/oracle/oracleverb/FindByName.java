package org.peacewing.oracle.oracleverb;

import java.util.ArrayList;
import java.util.List;

import org.peacewing.core.Verb;
import org.peacewing.core.Writeable;
import org.peacewing.domain.Domain;
import org.peacewing.domain.JavascriptBlock;
import org.peacewing.domain.JavascriptMethod;
import org.peacewing.domain.Method;
import org.peacewing.domain.Signature;
import org.peacewing.domain.Statement;
import org.peacewing.domain.Type;
import org.peacewing.domain.Var;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.generator.NamedStatementGenerator;
import org.peacewing.generator.NamedStatementListGenerator;
import org.peacewing.oracle.generator.MybatisOracleSqlReflector;
import org.peacewing.utils.InterVarUtil;
import org.peacewing.utils.StringUtil;
import org.peacewing.utils.WriteableUtil;

public class FindByName extends Verb implements EasyUIPositions {

	@Override
	public Method generateDaoImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("find"+this.domain.getStandardName()+"By"+StringUtil.capFirst(this.domain.getDomainName().getFieldName()));	
		method.setNoContainer(true);
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(100L,1,"<select id=\""+StringUtil.lowerFirst(this.getVerbName())+"\" parameterType=\"string\" resultMap=\""+this.domain.getLowerFirstDomainName()+"\">"));
		list.add(new Statement(200L,2, MybatisOracleSqlReflector.generateFindByNameStatement(domain)));
		list.add(new Statement(300L,1,"</select>"));method.setMethodStatementList(WriteableUtil.merge(list));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}
	
	@Override
	public String generateDaoImplMethodString() throws Exception{
		Method m = this.generateDaoImplMethod();
		String s = m.generateMethodString();
		return s;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("find"+this.domain.getStandardName()+"By"+StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
		method.setReturnType(new Type(this.domain.getStandardName(),this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,this.domain.getDomainName().getFieldName(),new Type("String")));
		method.setThrowException(true);
		
		return method;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception{
		return generateDaoMethodDefinition().generateMethodDefinition();
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception{
		Method m = this.generateDaoImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception{
		Method method = new Method();
		method.setStandardName("find"+this.domain.getStandardName()+"By"+StringUtil.capFirst(this.domain.getDomainName().getFieldName()));
		method.setReturnType(new Type(this.domain.getStandardName(),this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addSignature(new Signature(1,this.domain.getDomainName().getFieldName(),new Type("String")));
		method.setThrowException(true);
		
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception{
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateControllerMethod() throws Exception{
		return null;
	}

	@Override
	public String generateControllerMethodString() throws Exception{
		return null;
	}

	@Override
	public Method generateServiceImplMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("find"+this.domain.getStandardName()+"By"+this.domain.getDomainName().getCapFirstFieldName());
		method.setReturnType(new Type(this.domain.getStandardName(),this.domain.getPackageToken()));
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".dao."+this.domain.getStandardName()+"Dao");
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getDomainName().getFieldName(),new Type("String")));
		method.setThrowException(true);
		method.addMetaData("Override");
		
		Method daomethod = this.generateDaoMethodDefinition();
		
		List<Writeable> list = new ArrayList<Writeable>();
		list.add(new Statement(1000L,2, "return " + daomethod.generateStandardServiceImplCallString(InterVarUtil.DB.dao.getVarName())+";"));
		method.setMethodStatementList(WriteableUtil.merge(list));

		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception{
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception{
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public FindByName(){
		super();
		if (this.domain != null) this.setVerbName("Find"+this.domain.getStandardName()+"By"+StringUtil.capFirst(this.domain.getStandardName()+"Name"));
		else this.setVerbName("FindByName");
	}
	
	public FindByName(Domain domain){
		super();
		this.domain = domain;
		this.setVerbName("Find"+this.domain.getStandardName()+"By"+StringUtil.capFirst(this.domain.getStandardName()+"Name"));
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception{
		return null;
	}

	@Override
	public Method generateFacadeMethod() throws Exception{
		Method method = new Method();
		method.setStandardName("find"+this.domain.getStandardName()+"By"+this.domain.getDomainName().getCapFirstFieldName());
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport(this.domain.getPackageToken()+".domain."+this.domain.getStandardName());
		method.addAdditionalImport(this.domain.getPackageToken()+".service."+this.domain.getStandardName()+"Service");
		method.addSignature(new Signature(1,this.domain.getDomainName().getLowerFirstFieldName(),this.domain.getDomainName().getFieldRawType(), "","RequestParam(value = \""+this.domain.getDomainName().getLowerFirstFieldName()+"\", required = true)"));
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		Var resultMap = new Var("result", new Type("TreeMap<String,Object>","java.util"));
		Var domainVar = new Var(this.domain.getLowerFirstDomainName(), new Type(this.domain.getCapFirstDomainName(),this.domain.getPackageToken()+".domain."+this.domain.getCapFirstDomainName()));
		Var service = new Var("service", new Type(this.domain.getStandardName()+"Service",this.domain.getPackageToken()));		
		Method serviceMethod = this.generateServiceMethodDefinition();
		wlist.add(NamedStatementGenerator.getJsonResultMap(1000L, 2, resultMap));
		wlist.add(NamedStatementGenerator.getSpringMVCCallServiceMethodByDomainNameReturnDomain(2000L, 2,this.domain, service, serviceMethod));
		wlist.add(NamedStatementListGenerator.getPutJsonResultMapWithSuccessAndDomainVar(3000L, 2, resultMap,domainVar));
		wlist.add(new Statement(4000L, 2, "return " + resultMap.getVarName()+";"));	
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateFacadeMethodString() throws Exception{
		Method m = this.generateFacadeMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateFacadeMethodStringWithSerial() throws Exception{
		Method m = this.generateFacadeMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public JavascriptBlock generateEasyUIJSButtonBlock() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateEasyUIJSButtonBlockString() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentString();
	}

	@Override
	public String generateEasyUIJSButtonBlockStringWithSerial() throws Exception {
		return generateEasyUIJSButtonBlock().generateBlockContentStringWithSerial();
	}

	@Override
	public JavascriptMethod generateEasyUIJSActionMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateEasyUIJSActionString() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}

	@Override
	public String generateEasyUIJSActionStringWithSerial() throws Exception {
		return generateEasyUIJSActionMethod().generateMethodContentString();
	}


}
