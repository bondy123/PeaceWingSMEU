package org.peacewing.generator;

import org.peacewing.domain.Domain;
import org.peacewing.oracle.generator.Oracle11gSqlReflector;
import org.peacewing.utils.SqlReflector;

public class MysqlTwoDomainsDBDefinitionGenerator{
	protected Domain master;
	protected Domain slave;
	
	public MysqlTwoDomainsDBDefinitionGenerator(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
	}

	public String generateDBSql(String dbtype) throws Exception{
		StringBuilder sb = new StringBuilder();
		
		if ("Oracle".equalsIgnoreCase(dbtype)){
			sb.append(Oracle11gSqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		}else{
			sb.append(SqlReflector.generateLinkTableDefinition(this.master,this.slave)).append("\n");
		}
		return sb.toString();
	}
	
	public String generateDropLinkTableSql() throws Exception{
		StringBuilder sb = new StringBuilder();

		sb.append(Oracle11gSqlReflector.generateDropLinkTableSql(this.master,this.slave)).append("\n");
		return sb.toString();
	}
}
