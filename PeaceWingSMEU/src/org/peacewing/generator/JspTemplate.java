package org.peacewing.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.peacewing.core.Verb;
import org.peacewing.domain.Controller;
import org.peacewing.domain.Domain;
import org.peacewing.domain.MenuItem;
import org.peacewing.domain.StatementList;

public abstract class JspTemplate {
	protected Domain domain;
	protected List<Controller> controllers = new ArrayList<Controller>();
	protected List<Verb> verbs = new ArrayList<Verb>();
	protected String standardName;
	protected List<Domain> allDomainList;
	protected Set<MenuItem> menuItems = new TreeSet<MenuItem>();
	
	public List<Domain> getAllDomainList() {
		return allDomainList;
	}

	public void setAllDomainList(List<Domain> allDomainList) {
		this.allDomainList = allDomainList;
	}

	public abstract String generateJspString();
	
	public abstract StatementList generateStatementList();

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}


	public List<Controller> getControllers() {
		return controllers;
	}

	public void setControllers(List<Controller> controllers) {
		this.controllers = controllers;
	}

	public List<Verb> getVerbs() {
		return verbs;
	}

	public void setVerbs(List<Verb> verbs) {
		this.verbs = verbs;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Set<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(Set<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

}
