package org.peacewing.domain;

import java.io.Serializable;
import java.util.Comparator;

public class StringComparator implements Comparator,Serializable
{  
	private static final long serialVersionUID = -3127688396541157950L;

	public int compare(Object o1,Object o2)  
    {  
        String s1 = (String)o1;  
        String s2 = (String)o2;  
        return s1.compareTo(s2);
    }  
}  
