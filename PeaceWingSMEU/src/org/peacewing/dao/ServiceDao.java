package org.peacewing.dao;

import java.util.List;

import org.peacewing.domain.Domain;
import org.peacewing.domain.Interface;
import org.peacewing.domain.Method;
import org.peacewing.domain.Naming;

public interface ServiceDao {
	public Interface generateService(Naming naming, Domain domain,List<Method> methods) throws Exception;
	public Class generateServiceImpl(Naming naming, Domain domain,List<Method> methods) throws Exception;
}
