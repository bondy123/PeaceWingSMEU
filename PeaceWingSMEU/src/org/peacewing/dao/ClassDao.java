package org.peacewing.dao;

import org.peacewing.domain.Naming;
import org.peacewing.domain.Class;

public interface ClassDao {
	public Class generateClass(Naming naming, String standardName) throws Exception;
	public String generateClassString(Naming naming, String standardName) throws Exception;
}
