package org.javaforever.poitranslator.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.peacewing.compiler.SGSCompiler;
import org.peacewing.core.ApplicationContextXml;
import org.peacewing.core.SpringMVCFacade;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Dropdown;
import org.peacewing.domain.Field;
import org.peacewing.domain.Prism;
import org.peacewing.domain.Project;
import org.peacewing.domain.ValidateInfo;
import org.peacewing.exception.ValidateException;
import org.peacewing.generator.DBDefinitionGenerator;
import org.peacewing.generator.MysqlDBDefinitionGenerator;
import org.peacewing.oracle.generator.Oracle11gDBDefinitionGenerator;
import org.peacewing.utils.StringUtil;

public class ProjectExcelWorkBook{
   protected Project project;
    
   protected String username;
   protected String password;
   protected String filename;
   
   public Project translate(HSSFWorkbook book) throws Exception {
	   project = new Project();
	   project = translateProjectMetaData(book.getSheet("project"),project);
	   List<Domain> domainList = new ArrayList<Domain>();
	   
	   for (int i=1;i<book.getNumberOfSheets();i++){
		   HSSFSheet sheet = book.getSheetAt(i);
		   if (sheet.getSheetName().toLowerCase().contains("domain")) {
			   Domain domain = translateDomain(sheet); 
			   domain.setDbPrefix(project.getDbPrefix());
			   domain.setPackageToken(project.getPackageToken());
			   // output data 
			   ArrayList<Domain> dataList =  new ArrayList<Domain>();
			   dataList.addAll(readDomainListWithData(sheet,domain));
			   System.out.println("JerryDebug:datalistsize:"+dataList.size());
			   for (Domain d: dataList){
				   System.out.println(d.getDomainName().getFieldValue());
			   }
			   project.addDataDomains(dataList);
			   // end ouput
			   domainList.add(domain);
		   }
		   
	   }
	   project.setDomains(domainList);
	   
	   Project project2 = new Project(project.getStandardName(),project.getPackageToken(),project.getTechnicalstack(),
				project.getDbUsername(),project.getDbPassword(),project.isEmptypassword(),project.getDbName(),project.getDbType());
	   DBDefinitionGenerator dbdg = new MysqlDBDefinitionGenerator();
	   dbdg.setDbName(project.getDbName());
	   if (!StringUtil.isBlank(project)&&!StringUtil.isBlank(project.getDbType())&&project.getDbType().equalsIgnoreCase("oracle")) dbdg = new Oracle11gDBDefinitionGenerator();
	   ApplicationContextXml axml = new ApplicationContextXml();
	   
	   	SGSCompiler.decorateDropdowns(domainList);
		if (project.getPackageToken() == null || "".equals(project.getPackageToken())) {		
			ValidateInfo info = new ValidateInfo();
			info.addCompileError("没有设置PackageToken！");
			throw new ValidateException(info);
		} else {			
			String dbPrefix = project.getDbPrefix();
			for (int m=0;m<domainList.size();m++){
				domainList.get(m).setDbPrefix(dbPrefix);
				domainList.get(m).setPackageToken(project.getPackageToken());
			}
		}
		List<Prism> prismList = SGSCompiler.generatePrismsByDomains(domainList,dbdg,project2.getDbType());
		project2.setPrisms(prismList);
		TreeSet<SpringMVCFacade> myfacades = new TreeSet<SpringMVCFacade>();
		for (Prism p:prismList){
			myfacades.add(p.getFacade());
		}
		
		project2.setDomains(domainList);
		project2.setDataDomains(project.getDataDomains());
		dbdg.setDomains(domainList);
		
		axml.setDomainList(domainList);
		axml.setDbname(project2.getDbName());
		axml.setDbPassword(project2.getDbPassword());
		axml.setEmptypassword(project2.isEmptypassword());;
		axml.setDbType(project2.getDbType());
		
		axml.setFacades(myfacades);
		if (project.isEmptypassword()) {
			axml.setDbPassword("");
			axml.setEmptypassword(true);
		}
		List<String> packageToScanList = new ArrayList<String>();
		packageToScanList.add(project.getPackageToken()+".domain");
		axml.setPackagesToScanList(packageToScanList);
		axml.setPutInsideSrcAndClasses(true);
		
	
		project2.addDBDefinitionGenerator(dbdg);									
		project2.setPrisms(prismList);
		project2.replaceConfigFile(axml);

		return project2;
   }
   
   public Project translateProjectMetaData(HSSFSheet metaSheet,Project project){
	   String projectName = readMetaField(metaSheet,"project");
	   String packageToken = readMetaField(metaSheet,"packagetoken");
	   String dbprefix = readMetaField(metaSheet,"dbprefix");
	   String dbname = readMetaField(metaSheet,"dbname");
	   String dbusername = readMetaField(metaSheet,"dbusername");
	   String dbpassword = readMetaField(metaSheet,"dbpassword");
	   String dbtype = readMetaField(metaSheet,"dbtype");
	   String technicalstack = readMetaField(metaSheet,"technicalstack");
	   project.setStandardName(projectName);
	   project.setPackageToken(packageToken);
	   project.setDbPrefix(dbprefix);
	   project.setDbName(dbname);
	   project.setDbUsername(dbusername);
	   project.setDbPassword(dbpassword);
	   project.setDbType(dbtype);
	   project.setTechnicalstack(technicalstack);
	   return  project;	   
   }
   
   public Prism translatePrism(HSSFSheet prismSheet){
	   Prism prism = new Prism();
	   return    prism;
   }
   
   public Domain translateDomain(HSSFSheet domainSheet) throws ValidateException{
	   Domain domain = new Domain();
	   String domainName = readMetaField(domainSheet,"domain");
	   String plural = readMetaField(domainSheet,"plural");
	   String domainlabel = readMetaField(domainSheet,"domainlabel");
	   Cell metaFieldCell = locateKeyCell(domainSheet,"元字段类型");
	   Cell fieldCell = locateKeyCell(domainSheet,"字段");
	   Cell fieldTypeCell = locateKeyCell(domainSheet,"字段类型");
	   Cell fieldLabelCell = locateKeyCell(domainSheet,"字段标签");
	   Cell fieldDataCell = locateKeyCell(domainSheet,"数据");
	   for (int i=metaFieldCell.getColumnIndex()+1;i< domainSheet.getRow(metaFieldCell.getRowIndex()).getLastCellNum();i++){
		   String metaField = readFieldMeta(domainSheet,i,metaFieldCell.getRowIndex());
		   if (!StringUtil.isBlank(metaField)&&(metaField.equalsIgnoreCase("field")||metaField.equalsIgnoreCase("id")||metaField.equalsIgnoreCase("domainid")||metaField.equalsIgnoreCase("domainname")||metaField.equalsIgnoreCase("active")||metaField.equalsIgnoreCase("activefield"))){
			   Field f = readDomainField(domainSheet,i,metaFieldCell.getRowIndex(),fieldCell.getRowIndex(),fieldTypeCell.getRowIndex(),fieldLabelCell.getRowIndex(),domain);
			   if (!StringUtil.isBlank(metaField)&&metaField.equalsIgnoreCase("field")){
				   domain.addField(f);
			   }else if (!StringUtil.isBlank(metaField)&&(metaField.equalsIgnoreCase("id")||metaField.equalsIgnoreCase("domainid"))){
				   domain.setDomainId(f);
			   }else if (!StringUtil.isBlank(metaField)&&(metaField.equalsIgnoreCase("active")||metaField.equalsIgnoreCase("activefield"))){
				   domain.setActive(f);
			   }else if (!StringUtil.isBlank(metaField)&&(metaField.equalsIgnoreCase("domainname")||metaField.equalsIgnoreCase("activefield"))){
				   domain.setDomainName(f);
			   }
		   }else if (!StringUtil.isBlank(metaField)&&metaField.equalsIgnoreCase("dropdown")){
			   Dropdown dp = readDropdown(domainSheet,i,metaFieldCell.getRowIndex(),fieldCell.getRowIndex(),fieldTypeCell.getRowIndex(),fieldLabelCell.getRowIndex());
			   domain.addField(dp);
		   }else if (!StringUtil.isBlank(metaField)&&metaField.equalsIgnoreCase("manytomanyslave")){
			   String mtmname = readManyToManyName(domainSheet,i,metaFieldCell.getRowIndex(),fieldCell.getRowIndex(),fieldTypeCell.getRowIndex(),fieldLabelCell.getRowIndex());
			   domain.addManyToManySlaveName(mtmname);
		   }
	   }
	   domain.setStandardName(domainName);
	   if (!StringUtil.isBlank(plural)) domain.setPlural(plural);
	   if (!StringUtil.isBlank(domainlabel)) domain.setLabel(domainlabel);
	  
	   return domain;
   }
   
   public Field readDomainField(HSSFSheet sheet,int columIndex, int metaFieldIndex, int fieldIndex, int fieldTypeIndex,int fieldLabelIndex,Domain domain) throws ValidateException{
	   Field f = new Field();
	   String metafield = readFieldMeta(sheet,columIndex,metaFieldIndex);
	   System.out.println("JerryDebug:"+metafield);
	   String fieldname = sheet.getRow(fieldIndex).getCell(columIndex).getStringCellValue();
	   String fieldType = sheet.getRow(fieldTypeIndex).getCell(columIndex).getStringCellValue();
	   String fieldLabel = sheet.getRow(fieldLabelIndex).getCell(columIndex).getStringCellValue();
	   if (!StringUtil.isBlank(metafield)&&(metafield.equalsIgnoreCase("id")||metafield.equalsIgnoreCase("domianid")||metafield.equalsIgnoreCase("domainname")||metafield.equalsIgnoreCase("active")||metafield.equalsIgnoreCase("activefield")||metafield.equalsIgnoreCase("field"))){
		   if (!StringUtil.isBlank(fieldType)) f.setFieldType(fieldType);
		   if (!StringUtil.isBlank(fieldLabel)) {
			   f.setLabel(fieldLabel);
			   domain.putFieldLabel(fieldname, fieldLabel);
		   }
		   if (!StringUtil.isBlank(fieldname)) f.setFieldName(fieldname);
		   return f;
	   } else {
		   throw new ValidateException("字段解析错误");
	   }
   }
   
   public Dropdown readDropdown(HSSFSheet sheet,int columIndex, int metaFieldIndex, int fieldIndex, int fieldTypeIndex,int fieldLabelIndex) throws ValidateException{  
	   String metafield = sheet.getRow(metaFieldIndex).getCell(columIndex).getStringCellValue();
	   String fieldname = sheet.getRow(fieldIndex).getCell(columIndex).getStringCellValue();
	   String fieldType = sheet.getRow(fieldTypeIndex).getCell(columIndex).getStringCellValue();
	   String fieldLabel = sheet.getRow(fieldLabelIndex).getCell(columIndex).getStringCellValue();	   
	   if (metafield!=null && !metafield.equals("")&&metafield.equalsIgnoreCase("dropdown")){
		   Dropdown dp = new Dropdown(fieldType);
		   dp.setAliasName(fieldname);
		   dp.setLabel(fieldLabel);
		   return dp;
	   } else {
		   throw new ValidateException("字段解析错误");
	   } 
   }
   
   public String readFieldMeta(HSSFSheet sheet,int columIndex, int metaFieldIndex){  
	   String metafield = sheet.getRow(metaFieldIndex).getCell(columIndex).getStringCellValue();  
	   return metafield;
   }
   
   public String readManyToManyName(HSSFSheet sheet,int columIndex, int metaFieldIndex, int fieldIndex, int fieldTypeIndex,int fieldLabelIndex) throws ValidateException{  
	   String metafield = sheet.getRow(metaFieldIndex).getCell(columIndex).getStringCellValue();
	   String fieldType = sheet.getRow(fieldTypeIndex).getCell(columIndex).getStringCellValue();
	   if (metafield!=null && !metafield.equals("")&&metafield.equalsIgnoreCase("manytomanyslave")){
		   return fieldType;
	   } else {
		   throw new ValidateException("字段解析错误");
	   } 
   }
   
   public String readMetaField(HSSFSheet metaSheet,String key){
	   Cell c = locateKeyCell(metaSheet,key);
	   if (c==null) return "";
	   else return metaSheet.getRow(c.getRowIndex()).getCell(c.getColumnIndex()+1).getStringCellValue();  
   }
   
   public Cell locateKeyCell(HSSFSheet metaSheet, String key){
	   int rowbegin = metaSheet.getFirstRowNum();
	   int rowend = metaSheet.getLastRowNum();
	   for (int i=rowbegin;i<=rowend;i++){
		   Row r = metaSheet.getRow(i);
		   for (int j=r.getFirstCellNum();j<=r.getLastCellNum();j++){
			   Cell c = r.getCell(j);
			   if (c!=null && c.getStringCellValue().equalsIgnoreCase(key)) return c;
		   }		   
	   }
	   return null;
   }
   
   public List<Domain> readDomainListWithData(HSSFSheet sheet, Domain templateDomain){
	   List<Domain> resultList = new ArrayList<Domain>();
	   Cell dataCell = locateKeyCell(sheet,"数据");
	   Cell fieldCell = locateKeyCell(sheet,"字段");
	   for (int i=dataCell.getRowIndex();i<findOutLastDataRowIndex(sheet,findOutIdColIndex(sheet),dataCell.getRowIndex());i++){
		   Domain targetDomain = (Domain)templateDomain.deepClone();
		   for (Field f: templateDomain.getFields()){
			   if (f instanceof Dropdown){
				   Dropdown dp = (Dropdown) f;
				   String fieldValue = readDomainFieldValue(sheet,dp.getAliasName(),fieldCell.getColumnIndex()+1,fieldCell.getRowIndex(),i);
				   System.out.println("JerryDebug:dropdown:fieldvalue:"+fieldValue);
				   if (!StringUtil.isBlank(fieldValue)) targetDomain.setFieldValue(dp.getAliasName(),fieldValue);
				   else  targetDomain.setFieldValue(dp.getAliasName(),fieldValue);
				   
				   System.out.println("JerryDebug:dropdown:value:"+targetDomain.getField(dp.getAliasName()).getFieldValue());
			   }else {
				   String fieldValue = readDomainFieldValue(sheet,f.getFieldName(),fieldCell.getColumnIndex()+1,fieldCell.getRowIndex(),i);
				   if (!StringUtil.isBlank(fieldValue)) targetDomain.getField(f.getFieldName()).setFieldValue(fieldValue);
				   else targetDomain.getField(f.getFieldName()).setFieldValue("");	
			   }
		   }
		   resultList.add(targetDomain);
	   }
	   return resultList;
   }
   
   public int findOutIdColIndex(HSSFSheet sheet){
	   Cell metaFieldCell = locateKeyCell(sheet,"元字段类型");
	   for (int i= metaFieldCell.getColumnIndex()+1;i<sheet.getRow(metaFieldCell.getRowIndex()).getLastCellNum();i++){
		   if (sheet.getRow(metaFieldCell.getRowIndex()).getCell(i).getStringCellValue().equals("id")) {
			   return i;
		   }
	   }
	   return metaFieldCell.getColumnIndex()+1;
   }
   
   public int findOutLastDataRowIndex(HSSFSheet sheet,int idColIndex, int beginRowIndex){
	   for (int i=beginRowIndex;i<=sheet.getLastRowNum();i++){
		   if (StringUtil.isBlank(getCellStringValue(sheet.getRow(i).getCell(idColIndex)))) return i;
	   }
	   return sheet.getLastRowNum();
   }
   
   public String readDomainFieldValue(HSSFSheet sheet, String fieldName,int beginColIndex, int fieldNameRowIndex,int rowIndex){
	   for (int i=beginColIndex;i<sheet.getRow(fieldNameRowIndex).getLastCellNum();i++){
		   Cell c = sheet.getRow(fieldNameRowIndex).getCell(i);
		   String cellfieldName = c.getStringCellValue();
		   if (!StringUtil.isBlank(cellfieldName)&&cellfieldName.equals(fieldName)) {
			   return getCellStringValue(sheet.getRow(rowIndex).getCell(i));
		   }
	   }
	   return "";
   }
   
    /**
     * 读取excel文件中的用户名列,即第一列
     * @throws IOException 
     * */
    public String readUsername(String filesrc) throws IOException{
        List<String> userList = new ArrayList<String>();
        //读取excel文件
        InputStream is = new FileInputStream(filesrc);
        POIFSFileSystem fs = new POIFSFileSystem(is);
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        if(sheet==null){
            System.out.println("暂无数据，请输入测试数据");
        }
        //获取文件行数
        int rows = sheet.getLastRowNum();
        //获取文件列数
        /*int cols = sheet.getRow(0).getPhysicalNumberOfCells();
        //获取第一行的数据，一般第一行为属性值，所以这里可以忽略
        String colValue1 = sheet.getRow(0).toString();
        String colValues2 = sheet.getRow(1).toString();*/
        //取出第一列的用户名，去除掉第一行中的标题
        username = sheet.getRow(1).getCell(0).toString();
        System.out.println(username);
        return username;
    }
    
    /**
     * 获取第二列，得到密码list
     * @throws IOException 
     * */
    public String readPassword(String filesrc) throws IOException{
        //读取excel文件
        InputStream is = new FileInputStream(filesrc);
        POIFSFileSystem fs = new POIFSFileSystem(is);
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        HSSFSheet sheet = wb.getSheetAt(0);
        if(sheet==null){
            System.out.println("暂无数据，请输入测试数据");
        }
        //取出第二列的密码值，去掉第一行中的标题
        password = sheet.getRow(2).getCell(0).toString();
        System.out.println(password);
        return password;
    }
    
    public List<ActionGroup> getActionGroups(String filesrc) throws Exception{
    	File f = new File(filesrc);
    	this.filename = f.getName().split("\\.")[0];
    	System.out.println("JerryDebug:getRows:fileName:"+this.filename);
    	List<ActionGroup> actionGroup = new ArrayList<ActionGroup>();
        try (HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem( new FileInputStream(f)))){
        	Iterator<Sheet> iter = wb.sheetIterator();
        	while(iter.hasNext()){
        		HSSFSheet sheet = (HSSFSheet)iter.next();
        		List<Row> rowList = getRows(sheet);
        		ActionGroup ag = parseActions(rowList);
        		ag.setSheetName(sheet.getSheetName());
        		actionGroup.add(ag);
        	}
        }
        return actionGroup;
    }
    
    public List<Row> getRows(HSSFSheet sheet) throws Exception{
        if(sheet==null){
            System.out.println("暂无数据，请输入测试数据");
        }
    	int rowsHeight = sheet.getLastRowNum();
    	List<Row> result = new ArrayList<Row>();
    	//skip head row
    	for (int i=0;i<=rowsHeight;i++){
    		result.add(sheet.getRow(i));
    	}
    	return result;
    }   
    
    public void writeTestResultsToExcelFile(List<ActionGroup> actionGroups) throws Exception{
    	File f = new File(this.filename+"Result.xls");
    	if (!f.exists()) f.createNewFile();
    	else {
    		f.delete();
    		f.createNewFile();
    	}
    	System.out.println("JerryDebug:getRows:writeTestResultsToExcelFile:"+f.getName());
        try (OutputStream out = new FileOutputStream(f)){
        	HSSFWorkbook wb = new HSSFWorkbook();
        	for (ActionGroup ag:actionGroups){
		        HSSFSheet sheet = wb.createSheet(ag.getSheetName());
		        HSSFRow row0 = sheet.createRow(0);
		        setupResultRow(wb,row0,ag);
		        //HSSFRow row1 = sheet.createRow(1);
		        //setupHeadRow(row1,ag.getHeadRow());
		        List<Action> actions = ag.getActions();
		        for (int i=0;i<actions.size();i++){		        	 
	                 HSSFRow row = sheet.createRow(i + 1);
	                 HSSFCell c1 = row.createCell((short)1);
	                 HSSFCell c2 = row.createCell((short)2);
	                 HSSFCell c3 = row.createCell((short)3);
	                 HSSFCell c4 = row.createCell((short)4);
	                 HSSFCell c5 = row.createCell((short)5);
	                 HSSFCell c6 = row.createCell((short)6);
	                 HSSFCell c7 = row.createCell((short)7);
	                 c1.setCellType(CellType.STRING);
	                 c2.setCellType(CellType.STRING);
	                 c3.setCellType(CellType.STRING);
	                 c4.setCellType(CellType.STRING);
	                 c5.setCellType(CellType.STRING);
	                 c6.setCellType(CellType.STRING);
	                 c7.setCellType(CellType.STRING);
	                 
	                 c1.setCellValue(actions.get(i).getTestCaseSerial());  
	                 c2.setCellValue(actions.get(i).getTestCaseDescription());
	                 c3.setCellValue(actions.get(i).getName());	  
	                 c4.setCellValue(actions.get(i).getXpath());
	                 c5.setCellValue(actions.get(i).getValue());
	                 c6.setCellValue(actions.get(i).getIsIgnore());
	                 c7.setCellValue(actions.get(i).getResult());
		        }     
        	}
	        wb.write(out);  
	        wb.close();
        }
    }
    
    public void setupResultRow(HSSFWorkbook wb,HSSFRow row0,ActionGroup ag){
    	if (ag.success()) setupSuccessResultRow(wb,row0);
    	else setupFailureResultRow(wb,row0);
    }
    
    public void setupSuccessResultRow(HSSFWorkbook wb,HSSFRow row0){
   	 	HSSFCell c1 = row0.createCell((short)1);
        HSSFCell c2 = row0.createCell((short)2);
        HSSFCell c3 = row0.createCell((short)3);
        HSSFCell c4 = row0.createCell((short)4);
        HSSFCell c5 = row0.createCell((short)5);
        HSSFCell c6 = row0.createCell((short)6);
        HSSFCell c7 = row0.createCell((short)7);
        c1.setCellType(CellType.STRING);
        c2.setCellType(CellType.STRING);
        c3.setCellType(CellType.STRING);
        c4.setCellType(CellType.STRING);
        c5.setCellType(CellType.STRING);
        c6.setCellType(CellType.STRING);
        c7.setCellType(CellType.STRING);
        HSSFCellStyle style = wb.createCellStyle();
        
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.GREEN.index);
        c1.setCellStyle(style);  //cell 是 HSSFCell 对象
        c2.setCellStyle(style); 
        c3.setCellStyle(style); 
        c4.setCellStyle(style); 
        c5.setCellStyle(style); 
        c6.setCellStyle(style); 
        c7.setCellStyle(style);       

        c4.setCellValue("Success");
   }
    
    public void setupFailureResultRow(HSSFWorkbook wb,HSSFRow row0){
   	 	HSSFCell c1 = row0.createCell((short)1);
        HSSFCell c2 = row0.createCell((short)2);
        HSSFCell c3 = row0.createCell((short)3);
        HSSFCell c4 = row0.createCell((short)4);
        HSSFCell c5 = row0.createCell((short)5);
        HSSFCell c6 = row0.createCell((short)6);
        HSSFCell c7 = row0.createCell((short)7);
        c1.setCellType(CellType.STRING);
        c2.setCellType(CellType.STRING);
        c3.setCellType(CellType.STRING);
        c4.setCellType(CellType.STRING);
        c5.setCellType(CellType.STRING);
        c6.setCellType(CellType.STRING);
        c7.setCellType(CellType.STRING);
        HSSFCellStyle style = wb.createCellStyle();
        
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.RED.index);
        c1.setCellStyle(style);  //cell 是 HSSFCell 对象
        c2.setCellStyle(style); 
        c3.setCellStyle(style); 
        c4.setCellStyle(style); 
        c5.setCellStyle(style); 
        c6.setCellStyle(style); 
        c7.setCellStyle(style);       

        c4.setCellValue("Failure");
   }
    
    public void setupHeadRow(HSSFRow row1,HeadRow headRow){
    	 HSSFCell c1 = row1.createCell((short)1);
         HSSFCell c2 = row1.createCell((short)2);
         HSSFCell c3 = row1.createCell((short)3);
         HSSFCell c4 = row1.createCell((short)4);
         HSSFCell c5 = row1.createCell((short)5);
         HSSFCell c6 = row1.createCell((short)6);
         HSSFCell c7 = row1.createCell((short)7);
         c1.setCellType(CellType.STRING);
         c2.setCellType(CellType.STRING);
         c3.setCellType(CellType.STRING);
         c4.setCellType(CellType.STRING);
         c5.setCellType(CellType.STRING);
         c6.setCellType(CellType.STRING);
         c7.setCellType(CellType.STRING);
         
         c1.setCellValue(headRow.getHeaderArr()[0]);  
         c2.setCellValue(headRow.getHeaderArr()[1]);
         c3.setCellValue(headRow.getHeaderArr()[2]);	  
         c4.setCellValue(headRow.getHeaderArr()[3]);
         c5.setCellValue(headRow.getHeaderArr()[4]);
         c6.setCellValue(headRow.getHeaderArr()[5]);
         c7.setCellValue(headRow.getHeaderArr()[6]);
    }
    
     public ActionGroup parseActions(List<Row> rows){
    	 List<Action> result = new ArrayList<Action>();
    	 for (int i=0;i<rows.size();i++){
    		 if (i >= findHeadRowIndex(rows)){
	    		 Action ac = mapRowToAction(rows.get(i));
	    		 result.add(ac);
    		 }
    	 }
    	 ActionGroup actionGroup = new ActionGroup();
    	 actionGroup.setActions(result);
    	 return actionGroup;
     }
     
     public Integer findHeadRowIndex(List<Row> rows){
    	 for (int i=0;i<rows.size();i++){
    		 if (equalsHeadRow(rows.get(i))) return i;
    	 }
    	 return null;
     }
     
     public boolean equalsHeadRow(Row row){
    	 boolean result = true;
    	 for (int i=1;i< row.getLastCellNum()&&i-1<HeadRow.getHeaderArr().length;i++){
    		 if (row!=null&&row.getCell(i)!=null&&!getCellStringValue(row.getCell(i)).equals(HeadRow.getHeaderArr()[i-1])) result = false;
    	 }
    	 return result;
     }
     
     public String getCellStringValue(Cell c){    	 
    	 if (c.getCellType()==HSSFCell.CELL_TYPE_STRING) return c.getStringCellValue();
    	 else if (c.getCellType()==HSSFCell.CELL_TYPE_NUMERIC) {
    		 short format = c.getCellStyle().getDataFormat();
    		 if (format == 14 || format == 31 || format == 57 || format == 58) {
        		 DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");  
                 Date date = DateUtil.getJavaDate(c.getNumericCellValue());  
                 String value = formater.format(date);  
        		 return value;
        	 } else if (format == 20 || format == 32) {
        		 DateFormat formater = new SimpleDateFormat("HH:mm");    
                 Date date = DateUtil.getJavaDate(c.getNumericCellValue());  
                 String value = formater.format(date);
                 return value;
        	 }
    		 return ""+c.getNumericCellValue();
    	 }
    	 else if (c.getCellType()==HSSFCell.CELL_TYPE_BOOLEAN) return ""+c.getBooleanCellValue();
    	 else return "";
     }
     
     public Action mapRowToAction(Row row){
    	 Action ac = new Action();
    	 String serial = "";
    	 String desc ="";
    	 String action="";
    	 String xpath="";
    	 String value="";
    	 String ignore="";
    	 
    	 if(row.getCell(1)!=null) serial = row.getCell(1).toString();
    	 if(row.getCell(2)!=null) desc = row.getCell(2).toString();
    	 if(row.getCell(3)!=null) action = row.getCell(3).toString();
    	 if(row.getCell(4)!=null) xpath = row.getCell(4).toString();
    	 if(row.getCell(5)!=null) value = row.getCell(5).toString();
    	 if(row.getCell(6)!=null) ignore = row.getCell(6).toString();
    	 ac.setTestCaseSerial(serial);
    	 ac.setTestCaseDescription(desc);
    	 ac.setName(action);
    	 ac.setXpath(xpath);
    	 ac.setValue(value);
    	 ac.setIsIgnore(ignore);
    	 return ac;    	 
     }
     
     public static void main(String[] args){
    	 try {
	    	 InputStream is = new FileInputStream("C:\\\\Users\\\\jerry.shen03\\\\git\\\\PeaceWingSMEU\\\\PeaceWingSMEU\\\\src\\\\org\\\\javaforever\\\\poitranslator\\\\core\\\\GenerateSample.xls");
	         POIFSFileSystem fs = new POIFSFileSystem(is);
	         HSSFWorkbook wb = new HSSFWorkbook(fs);
	         
	         ProjectExcelWorkBook pwb = new ProjectExcelWorkBook();
	         Project pj = pwb.translate(wb);
	         //System.out.println(pj.toString());
	         System.out.println("=============");
	         System.out.println(pj.getStandardName());
	         System.out.println(pj.getPackageToken());
	         System.out.println(pj.getDbPrefix());
		  	 System.out.println(pj.getDbName());
		  	 System.out.println(pj.getDbUsername());
		  	 System.out.println(pj.getDbPassword());
		     System.out.println(pj.getDbType());
		     System.out.println(pj.getTechnicalstack());
		     
		     List<Domain> ds = pj.getDomains();
		     for (Domain d:ds){
		    	 System.out.println("++++++++++++++++++++");
		    	 System.out.println(d.getStandardName());
		    	 System.out.println(d.getPlural());
		    	 System.out.println(d.getLabel());
		    	 for (Field f:d.getFields()){
		    		 System.out.println(f.getFieldName());
		    	 }
		    	 for (String mtmname:d.getManyToManySlaveNames()){
		    		 System.out.println(mtmname);
		    	 }
		     }
		     
//		     List<Prism> ps = pj.getPrisms();
//		     for (Prism p:ps){
//		    	 System.out.println("---------------------");
//		    	 System.out.println(p.toString());
//		     }
    	 } catch (Exception e){
    		 e.printStackTrace();
    	 }
     }

}

