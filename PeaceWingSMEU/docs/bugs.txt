1.默认数据库名问题【已修复】
2.BooleanUtil (java)【已修复】
3.parseBoolean(js)【已修复】
4.基础类型用大写类【已修复】
5.域对象类名的链接，应该使用getText【已修复】
6.面板题名的null【已修复】
7.Grid脚注的NaN【已修复】
8.生成器界面【已修复】
9.几个新动词调通【已修复】
10.有几个新的Count动词【已修复】
11.update的Boolean类型字段传值不对【已修复】
12.生成物使用类类型【已修复】
13.生成器中文支持【已修复】
14.技术栈改名SMEU【已修复】
15.Compile Error 和Compile Warning修复【已修复】
16.支持用类类型声明字段【已修复】
17.js parseBoolean【已修复】
18.js isBlank【已修复】
19.GenerateAdvanced Integer.Integer.parseInt issue【已修复】
20.EU Homepage中英文兼顾【已修复】
21.dropdown的Label
22.活跃字段为deleted时，标签应该是删除【已修复】
23.新建数据表时要加上删除联系表的语句【已修复】
24.示例增强，加上所用的Dropdown
26.多对多管理列出似乎有问题，是Boolean值问题【GenerateOracleAdvanced已修复】
27.域对象要实现comparable接口【GenerateOracleAdvanced已修复】
28.GenerateOracleAdvanced示例Assign有问题，类型转换有问题，编译错【已修复】
29.GenerateOracle示例复杂动词的关系ID有类型问题[目视已修复，待测试]
30.分页参数应该用数字位置参数而不是文字参数【已修复】