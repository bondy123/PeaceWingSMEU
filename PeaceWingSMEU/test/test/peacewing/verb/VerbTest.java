package test.peacewing.verb;

import org.junit.Test;
import org.peacewing.domain.Domain;
import org.peacewing.domain.Field;
import org.peacewing.easyui.EasyUIPositions;
import org.peacewing.limitedverb.CountAllPage;
import org.peacewing.verb.Add;
import org.peacewing.verb.Delete;
import org.peacewing.verb.DeleteAll;
import org.peacewing.verb.ListAllByPage;
import org.peacewing.verb.SearchByFieldsByPage;
import org.peacewing.verb.SoftDelete;
import org.peacewing.verb.SoftDeleteAll;
import org.peacewing.verb.Toggle;
import org.peacewing.verb.ToggleOne;

public class VerbTest {
	@Test
	public void testListAllByPage()  throws Exception{
		ListAllByPage listAllByPage = new ListAllByPage();
		CountAllPage countAllPage = new CountAllPage();
		listAllByPage.setCountAllPage(countAllPage);
		
		Domain domain = getClockRecord();//getBonus();
		
		listAllByPage.setDomain(domain);
		countAllPage.setDomain(domain);
		
		System.out.println(listAllByPage.generateDaoImplMethodStringWithSerial());
		System.out.println("========================Dao Definintion===========");
		System.out.println(listAllByPage.generateDaoMethodDefinitionString());
		System.out.println("========================ServiceImpl===========");
		System.out.println(listAllByPage.generateServiceImplMethodStringWithSerial());
		System.out.println("========================Service===============");
		System.out.println(listAllByPage.generateServiceMethodDefinitionString());
		System.out.println("========================Facade===========");
		System.out.println(listAllByPage.generateFacadeMethodString());
		System.out.println("========================FacadeWithSerial===========");
		System.out.println(listAllByPage.generateFacadeMethodStringWithSerial());
		
		EasyUIPositions eups = (EasyUIPositions) listAllByPage;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());
	}
	
	@Test
	public void testAdd()  throws Exception{
		Add add = new Add();
		Domain d = getClockRecord();//getBonus();
		add.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) add;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());
		
	}
	
	@Test
	public void testDelete()  throws Exception{
		Delete delete = new Delete();
		Domain d = getClockRecord();//getBonus();
		delete.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) delete;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());
		
	}
	
	@Test
	public void testDeleteAll()  throws Exception{
		DeleteAll deleteAll = new DeleteAll();
		Domain d = getClockRecord();//getBonus();
		deleteAll.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) deleteAll;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());		
	}
	
	@Test
	public void testSoftDeleteAll()  throws Exception{
		SoftDeleteAll softDeleteAll = new SoftDeleteAll();
		Domain d = getClockRecord();//getBonus();
		softDeleteAll.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) softDeleteAll;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());		
	}
	
	@Test
	public void testSearchByFieldsByPage()  throws Exception{
		SearchByFieldsByPage search = new SearchByFieldsByPage();
		Domain d = getBonus();
		search.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) search;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());		
	}
	
	@Test
	public void testSoftDelete()  throws Exception{
		SoftDelete softDdelete = new SoftDelete();
		Domain d = getClockRecord();//getBonus();
		softDdelete.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) softDdelete;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());		
	}
	
	@Test
	public void testToggle()  throws Exception{
		Toggle toggle = new Toggle();
		Domain d = getClockRecord();//getBonus();
		toggle.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) toggle;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());		
	}
	
	@Test
	public void testToggleOne()  throws Exception{
		ToggleOne toggleOne = new ToggleOne();
		Domain d = getClockRecord();//getBonus();
		toggleOne.setDomain(d);
		
		EasyUIPositions eups = (EasyUIPositions) toggleOne;
		System.out.println("========================generateEasyUIJSActionStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSActionStringWithSerial());
		System.out.println("========================generateEasyUIJSButtonBlockStringWithSerial===========");
		System.out.println(eups.generateEasyUIJSButtonBlockStringWithSerial());		
	}
	
	public Domain getBonus(){		
		Domain domain = new Domain();
		domain.setPackageToken("org.peacewing");
		domain.setStandardName("Bonus");
		domain.setPlural("Bonuses");
		domain.addField("empid","long");
		domain.addField("userid", "long");
		domain.addField("reason", "String");
		domain.addField("bonusBalance", "double");
		domain.addField("description", "String");
		domain.setDomainId(new Field("id", "long"));
		domain.setDomainName(new Field("bonusName","String"));
		domain.setActive(new Field("active", "boolean"));
		return domain;
	}
	
	public Domain getClockRecord(){		
		Domain domain = new Domain();
		domain.setPackageToken("org.peacewing");
		domain.setStandardName("ClockRecord");
		domain.addField("empid","long");
		domain.addField("userid", "long");
		domain.addField("timeStamp", "String");
		domain.addField("description", "String");
		domain.setDomainId(new Field("id", "long"));
		domain.setDomainName(new Field("clockRecordName","String"));
		domain.setActive(new Field("active", "boolean"));
		return domain;
	}
	
}
